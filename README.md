# [快速指南] 使用Webhook連接器傳送訊息到Teams頻道(Channel)

## [目的]

使用Webhook連接器可以將訊息或通知，快速地公佈在Teams的頻道。讓在頻道中的團隊成員能夠迅速瞭解狀況，並能適時地作出回應。

## [任務]

1. 建立Teams測試用團隊
2. 建立Webhook連接器
3. 測試連接器

## [執行步驟]

1. 登入Teams，點選**團隊** > **加入或建立團隊**。

 ![001.png](./pics/001.png)…

 > 團隊是組織裡的一群人聚在一起，共同完成一件大事。 有時候指的是整個組織。

 > 團隊由頻道組成，頻道是您和團隊成員擁有的交談。 每個頻道都有專屬的特定主題、部門或專案。

2. 點選**建立團隊**。

 ![002.png](./pics/002.png)

 選擇**從頭建立團隊**。

 ![003.png](./pics/003.png)

 選擇**私人**進行封閉測試使用。

 ![004.png](./pics/004.png)

 輸入有關**團隊名稱**及**團隊描述**。

 ![005.png](./pics/005.png)

 增加**團隊成員**。

 ![006.png](./pics/006.png)

 團隊建立完成。

 ![007.png](./pics/007.png)

3. 開啟**一般**頻道的**設定(•••)**，選擇**連接器**。

 ![008.png](./pics/008.png)

 選擇**傳入Webhook**並**新增**。

 ![009.png](./pics/009.png)

 閱讀Incoming Webhook說明，並選擇**新增。

 ![010.png](./pics/010.png)

 提供Incoming Webhook一個識別名稱。

 ![011.png](./pics/011.png)

 上傳Incoming Webhook自訂影像。

 ![012.png](./pics/012.png)

 點選**建立**並等待數秒。

 ![013.png](./pics/013.png)

 會產生一組唯一的URL，該URL將對應此頻道。點選**複製**圖示複製連結至剪貼簿。後續測試將會需要此URL。選擇**完成**。

 ![014.png](./pics/014.png)

 回到頻道頁面，可看到剛剛建立Webhook的訊息。

 ![015.png](./pics/015.png)


4. Webhook連接器使用 `HTTP POST` 來傳送訊息的，所以直接透過 `curl` 進行資料傳送測試。

 請輸入以下命令。

 ```bash
   $ demoWebhookUrl='https://outlook.office.com/webhook/fc8......48e'

   $ curl -s -X POST -H 'Content-Type: application/json' -d '{"text": "Hello World"}' ${demoWebhookUrl}
 ```

5. 如果curl執行沒有錯誤，回到頻道頁面就可以看到測試訊息。

 ![016.png](./pics/016.png)

## [深入淺出]

1. 傳入訊息加上 `title`。

 ```bash
  $ curl -s -X POST ${demoWebhookUrl} \
    -H 'Content-Type: application/json' \
    -d '{"title": "今日公告", "text": "機房UPS故障，請各管理員掌握設備狀態"}'
 ```

 ![017.png](./pics/017.png)

2. `text` 內容可以使用 **[Markdown]** 格式顯示。

 ```bash
   $ curl -s -X POST ${demoWebhookUrl} \
     -H 'Content-Type: application/json' \
     -d '{"title": "Google Meet免費使用", "text": "直到**9月30日**都能免費全功能使用![Google Meet](https://www.gstatic.com/meet/google_meet_horizontal_wordmark_icon_40dp_ca224cf421a1cad730d46514bc52cee4.png)。請點選[連結](http://meet.google.com \"Google Meet\")"}'
 ```

 ![018.png](./pics/018.png)

3. 使用 `JSON檔` 作為資料輸入。

 ```JSON
 {
  "summary": "Card \"Test card\"",
  "themeColor": "0078D7",
  "title": "Card created: \"Name of card\"",
  "sections": [
    {
      "activityTitle": "David Claux",
      "activitySubtitle": "9/13/2016, 3:34pm",
      "activityImage": "https://connectorsdemo.azurewebsites.net/images/MSC12_Oscar_002.jpg",
      "facts": [
        {
          "name": "Board:",
          "value": "Name of board"
        },
        {
          "name": "List:",
          "value": "Name of list"
        },
        {
          "name": "Assigned to:",
          "value": "(none)"
        },
        {
          "name": "Due date:",
          "value": "(none)"
        }
      ],
      "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
    }
  ]
 }
 ```

 ![019.png](./pics/019.png)

---
## [問題]

> 無法使用Webhook方式將訊息傳入至**聊天**。

---
參考網站連結

- [Sending messages to connectors and webhooks](https://docs.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/connectors-using "")

- [Post external requests to Teams with incoming webhooks](https://docs.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook "")

- [Legacy actionable message card reference](https://docs.microsoft.com/en-us/outlook/actionable-messages/message-card-reference#card-examples "")

- [Sending Messages to MS Teams from PowerShell just got easier and better](https://evotec.xyz/sending-to-microsoft-teams-from-powershell-just-got-easier-and-better/ "")

- [How to send a MS Teams message using PowerShell 7](https://petri.com/how-to-send-a-microsoft-teams-message-using-powershell-7 "")

- [Announcing “30 Days of Microsoft Graph” Blog Series](https://developer.microsoft.com/en-us/graph/blogs/announcing-30-days-of-microsoft-graph-blog-series/# "")
---
[[回到首頁]](./READMD.md "HOME")
